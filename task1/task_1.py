result_string = str()

for element in range(2000, 3200+1):         #   range is [2000-3200]
    if element % 7 == 0 and element % 5 != 0:   #   divisible by 7 and not a multiple of 5
        result_string += str(element) + ", "     #       append string with comma separation

print(result_string[:-2])    #   print string except last two characters containing a comma and a space
