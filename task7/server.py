import time
import json
import asyncio
import websockets
import argparse
import os
from constants import *
from log_functions import *

parser = argparse.ArgumentParser(
    description='Server app to recvice WS messages and echo them with some addional info')
parser.add_argument("-c", "--disable-console-log",
                    action="store_false",
                    help='console logging enable/disable flag')
parser.add_argument("-f", "--enable-file-log",
                    action='store_true',
                    help='file logging enable/disable flag')
args = parser.parse_args()

async def echo(websocket):

    if os.path.isfile(MSG_LATENCY_STORAGE_FILE):
        os.remove(MSG_LATENCY_STORAGE_FILE)

    async for message in websocket:

        log(("DEBUG", args.disable_console_log, args.enable_file_log),
            SERVER, "Recieved data : ", message)

        msg_json = dict(json.loads(message))
        msg_json["rx_time"] = time.time_ns()
        msg_json["msg_latency"] = msg_json["rx_time"] - msg_json["tx_time"]

        log(("DEBUG", args.disable_console_log, args.enable_file_log),
            SERVER, "Sending data : ", msg_json)

        await websocket.send(json.dumps(msg_json))
        with open(MSG_LATENCY_STORAGE_FILE, APPEND_MODE) as latency_file:
            latency = "{}\n".format(str(msg_json["msg_latency"]))
            latency_file.write(latency)


async def main():
    async with websockets.serve(echo, WS_ADDRESS, WS_PORT):
        await asyncio.Future()  # run forever

asyncio.run(main())
