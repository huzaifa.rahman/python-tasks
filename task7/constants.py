MSG_FILE_NAME = "client_messages.json"
READ_MODE = "r"
APPEND_MODE = "a"
WRITE_MODE = "w"

CLIENT_LOGGING_LEVEL = "INFO"
CLIENT_LOG_FILE = "client.log"

SERVER_LOGGING_LEVEL = "DEBUG"
SERVER_LOG_FILE = "server.log"
MSG_LATENCY_STORAGE_FILE = "msg_latency.txt"

json_file_path = "./"

WS_ADDRESS = "localhost"
WS_PORT = "8765"
WS_COMPLETE_ADDRESS = f"ws://{WS_ADDRESS}:{WS_PORT}"

CLIENT = True
SERVER = False
