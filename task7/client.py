import time
import asyncio
import websockets
import json
import argparse
from constants import *
from log_functions import *

# ASSUMPTIONS
# JSON msg path is required


parser = argparse.ArgumentParser(
    description='Client app to send WS messages')
parser.add_argument("json_path",
                    type=str,
                    help='Path to json message file')
parser.add_argument("-c", "--disable-console-log",
                    action="store_false",
                    help='console logging enable/disable flag')
parser.add_argument("-f", "--enable-file-log",
                    action="store_true",
                    help='file logging enable/disable flag')
args = parser.parse_args()


async def simple_msg():
    async with websockets.connect(WS_COMPLETE_ADDRESS) as websocket:
        
        log(("INFO", args.disable_console_log, args.enable_file_log),
            CLIENT, "Connected to : ", WS_COMPLETE_ADDRESS)

        with open(args.json_path+MSG_FILE_NAME, READ_MODE) as msg_file:

            log(("INFO", args.disable_console_log, args.enable_file_log), CLIENT,
                "File opened : ", (args.json_path+MSG_FILE_NAME))
            
            json_msg = list(json.load(msg_file))
            
            log(("INFO", args.disable_console_log, args.enable_file_log),
                CLIENT, "Messages loaded locally", "")
            log(("INFO", args.disable_console_log, args.enable_file_log),
                CLIENT, "Starting sending data", "")
        
        for index in range(len(json_msg)):
            json_msg[index]["tx_time"] = time.time_ns()

            log(("DEBUG", args.disable_console_log, args.enable_file_log),
                CLIENT, "Sending data : ", json_msg[index])
            
            await websocket.send(json.dumps(json_msg[index]))
            received_msg = await websocket.recv()
            
            log(("DEBUG", args.disable_console_log, args.enable_file_log),
                CLIENT, "Recieved data : ", received_msg)

asyncio.run(simple_msg())   # Execute the coroutine coro and return the result
