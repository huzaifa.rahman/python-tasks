from constants import *
import datetime
import time

LOGGING_LEVEL = 0
CONSOLE_LOG = 1
FILE_LOG = 2


def log(logging_info, client, message, data):
    if client == True:
        if logging_info[LOGGING_LEVEL] == "INFO" and (CLIENT_LOGGING_LEVEL == "INFO" or CLIENT_LOGGING_LEVEL == "DEBUG"):
            log_msg = "{} : {} CLIENT ==> {} === {}\n".format(
                logging_info[LOGGING_LEVEL],
                datetime.datetime.fromtimestamp(
                    time.time()).strftime('%Y-%m-%d %H:%M:%S'),
                message, data)
        elif logging_info[LOGGING_LEVEL] == "DEBUG" and CLIENT_LOGGING_LEVEL == "DEBUG":
            log_msg = "{} : {} CLIENT ==> {} === {}\n".format(
                logging_info[LOGGING_LEVEL],
                datetime.datetime.fromtimestamp(
                    time.time()).strftime('%Y-%m-%d %H:%M:%S'),
                message, data)
        else:
            return

        if logging_info[CONSOLE_LOG]:
            print(log_msg)
        if logging_info[FILE_LOG]:
            with open(CLIENT_LOG_FILE, APPEND_MODE) as client_log_file:
                client_log_file.write(log_msg)
    else:
        if logging_info[LOGGING_LEVEL] == "INFO" and (SERVER_LOGGING_LEVEL == "INFO" or SERVER_LOGGING_LEVEL == "DEBUG"):
            log_msg = "{} : {} SERVER ==> {} === {}\n".format(
                logging_info[LOGGING_LEVEL],
                datetime.datetime.fromtimestamp(
                    time.time()).strftime('%Y-%m-%d %H:%M:%S'),
                message, data)
        elif logging_info[LOGGING_LEVEL] == "DEBUG" and SERVER_LOGGING_LEVEL == "DEBUG":
            log_msg = "{} : {} SERVER ==> {} === {}\n".format(
                logging_info[LOGGING_LEVEL],
                datetime.datetime.fromtimestamp(
                    time.time()).strftime('%Y-%m-%d %H:%M:%S'),
                message, data)
        else:
            return

        if bool(logging_info[CONSOLE_LOG]):
            print(log_msg)
        if logging_info[FILE_LOG]:
            with open(SERVER_LOG_FILE, APPEND_MODE) as server_log_file:
                server_log_file.write(log_msg)
