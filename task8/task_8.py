import argparse
import time
import datetime
import statistics
import numpy as np

LATENCY_FILE = "msg_latency.txt"
THOUSAND = 1000     # to convert nano seconds into micro

parser = argparse.ArgumentParser(
    description='Tool to calculate WS messages latency stats')
parser.add_argument("latency_data_file",
                    type=str,
                    help='Path to latency data file')
args = parser.parse_args()


def print_separation_line():    # simple separator
    print("---------------------------")

def print_formatted_data(message, data, use_us):
    print("{:<30}:\t{:<10} us".format(message, data)) if use_us else print("{:<30}:\t{}".format(message, data))     # wheather to print us or not


def print_header():     # to print header
    print_separation_line()
    print_formatted_data("Date-Time", datetime.datetime.fromtimestamp(
        time.time()).strftime('%d-%m-%Y-%H-%M-%S'), False)
    print("Stats for Websocket Latency")
    print_separation_line()

def main():
    print_header()
    with open(args.latency_data_file+LATENCY_FILE, "r") as latency_data_file:
        Lines = latency_data_file.readlines()   # read all lines in a list
        int_list = list()
        for line in Lines:
            int_list.append(int(line))  # convert the list into int list
        print_formatted_data("Mean", round(statistics.mean(int_list)/THOUSAND, 3), True)
        print_formatted_data("Median", round(statistics.median(int_list)/THOUSAND, 3), True)
        print_formatted_data("Min", round(min(int_list)/THOUSAND, 3), True)
        print_formatted_data("Max", round(max(int_list)/THOUSAND, 3), True)
        print_formatted_data("Standard Deviation", round(statistics.stdev(int_list)/THOUSAND, 3), True)

        print_formatted_data("90th percentile", round(np.percentile(int_list, 90)/THOUSAND, 3), False)
        print_formatted_data("99th percentile", round(np.percentile(int_list, 99)/THOUSAND, 3), False)
        print_formatted_data("99.9th percentile", round(np.percentile(int_list, 99.9)/THOUSAND, 3), False)
        print_formatted_data("99.99th percentile", round(np.percentile(int_list, 99.99)/THOUSAND, 3), False)
        print_formatted_data("99.999th percentile", round(np.percentile(int_list, 99.999)/THOUSAND, 3), False)
        print_formatted_data("99.999th percentile", round(np.percentile(int_list, 99.999)/THOUSAND), False)

if __name__ == "__main__":
    main()
