# PSUODOCODE
# make a file at the desired location
# get system info one by one
# format and append the details in file

from cpuinfo import get_cpu_info
import os
import sys
import getpass
import subprocess
import psutil

DIR_PATH = f"/home/{getpass.getuser()}/Details"
FILE_PATH = f"{DIR_PATH}/Summary.txt"
APPEND_MODE = "a"
READ_MODE = "r"
WRITE_PATH = "w"


def check_if_dir_exists():
    if not os.path.exists(DIR_PATH):
        os.mkdir(DIR_PATH)


def check_if_file_exists():
    if os.path.exists(FILE_PATH):
        open(FILE_PATH, WRITE_PATH).close()


def get_sys_data():
    sys_data = dict()
    cpu_info = dict(get_cpu_info().items())

    sys_data["Byte Order (Endiance)"] = sys.byteorder.capitalize()
    sys_data["Core(s) per socket"] = int(subprocess.check_output(
        "lscpu | grep 'socket' | awk '{print $4}' ", shell=True))
    sys_data["Model name"] = cpu_info.get("brand_raw")
    sys_data["CPU MHz"] = float(subprocess.check_output(
        " lscpu | grep 'CPU MHz' | awk '{print $3}' ", shell=True))
    sys_data["CPU max MHz"] = psutil.cpu_freq()[2]
    sys_data["CPU min MHz"] = psutil.cpu_freq()[1]
    sys_data["Virtualization Support"] = str(subprocess.check_output(
        " lscpu | grep 'Virtual' | awk '{print $2}'", shell=True)).replace("b'", '').replace("\\n'", '')
    sys_data["L1 (KBs)"] = cpu_info.get("l1_data_cache_size").split()[0]
    sys_data["L2 cache (KBs)"] = cpu_info.get("l2_cache_size").split()[0]
    sys_data["L3 cache (KBs)"] = int(cpu_info.get("l3_cache_size")/1000)
    sys_data["RAM Memory (GBs)"] = int(
        psutil.virtual_memory().total/1000000000)

    return sys_data


def write_single_line_to_file(message, data):
    with open(FILE_PATH, APPEND_MODE) as summary_file:
        summary_file.write("{:<30}:\t{}\n".format(message, data))


def write_data_to_file():
    check_if_dir_exists()
    check_if_file_exists()
    sys_data = get_sys_data()
    for key in sys_data:
        write_single_line_to_file(key, sys_data[key])

# open and read the file after the appending:


def print_summary_from_file():
    with open(FILE_PATH, READ_MODE) as summary_file:
        print(summary_file.read())


def main():
    write_data_to_file()
    print_summary_from_file()


main()

# if shell is True, the specified command will be executed through the shell. This can be useful if you are using Python primarily for the enhanced control flow it offers over most system shells and still want convenient access to other shell features such as shell pipes, filename wildcards, environment variable expansion, and expansion of ~ to a user’s home directory. However, note that Python itself offers implementations of many shell-like features
