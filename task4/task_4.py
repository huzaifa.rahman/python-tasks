import random
import argparse
import json

# ASSUMPTIONS
# Expects JSON data in correct format

#  getting arguments
parser = argparse.ArgumentParser(
    description='Estimates PI using Monte Carlos method')
parser.add_argument("-i",
                    type=int,
                    default=10000,
                    help='no. of iterations')
parser.add_argument("-j",
                    action="store_true",
                    help='flag to specify if input iterations is get from JSON')
args = parser.parse_args()

# checking for positivity
if args.i <= 0:
    raise argparse.ArgumentTypeError(
        "Invalid input, no. of iterations can only be positive")

circle_points = square_points = interval = rand_a = rand_b = 0
if args.j:
    with open("data_file.json", "r") as read_file:
        data = json.load(read_file)
        NO_OF_ITERATIONS = range(data["iterations"])
else:
    NO_OF_ITERATIONS = range(args.i)

for iterator in NO_OF_ITERATIONS:
    rand_a = random.random()
    rand_b = random.random()
    if rand_a**2 + rand_b**2 <= 1:
        circle_points += 1
    square_points += 1

PI = 4*(circle_points/square_points)

print(f"Estimated PI : {PI}")

