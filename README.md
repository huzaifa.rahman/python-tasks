# **Python Tasks**

This repo contains python practice tasks.

## **Steps to run the code**

- **Clone** the repo with ``git clone https://gitlab.com/huzaifa.rahman/python-tasks.git`` and navigate to the cloned repo with ``cd``.
- **Activate** the virtual environment with ``virtualenv <env_name> && source <env_name>/bin/activate`` where ``<env_name>`` is the name of the virtual environment. To **deactivate** simply run ``deactivate``.
- Then go into each tasks folder and run ``python <task_script>`` to run the task.
