from math_functions import *
from plot_function import *
import matplotlib.pyplot as plt

my_list = []
for element in range(1, 10+1):
    my_list.append(element)

plot(1,
     "Square plot of natural numbers from [1-10]", "Natural numbers", "Square of natural numbers",
     my_list,
     square(my_list))

plot(2,
     "Cube plot of natural numbers from [1-10]", "Natural numbers", "Cube of natural numbers",
     my_list,
     cube(my_list))

plt.show()
