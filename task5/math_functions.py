def square(my_list):
    squared_list = list()
    for element in my_list:
        squared_list.append(element**2)
    return squared_list

def cube(my_list):
    cubed_list = list()
    for element in my_list:
        cubed_list.append(element**3)
    return cubed_list