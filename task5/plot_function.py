import matplotlib.pyplot as plt

def plot(figure_num, title, xlabel, ylabel, x_cordinates, y_cordinates):
    plt.figure(figure_num)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.plot(x_cordinates, y_cordinates)