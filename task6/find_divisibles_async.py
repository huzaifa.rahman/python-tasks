from log_function import *
import time
import asyncio
from pprint import pprint


async def async_find_divisibles(in_range, divisor):
    divisable_list = list()
    Log(in_range, divisor, 0)

    start = time.time()
    for element in range(in_range):
        if element % divisor == 0:
            divisable_list.append(element)
        if element % 500000 == 0:
            await asyncio.sleep(0.0001)
    end = time.time()
    
    Log(in_range, divisor, end - start)
    return divisable_list


async def main():
    divs1 = loop.create_task(async_find_divisibles(50800000, 34113))    #  Schedule the execution of a Coroutines. Return a Task object.
    divs2 = loop.create_task(async_find_divisibles(100052, 3210))
    divs3 = loop.create_task(async_find_divisibles(500, 3))
    await asyncio.wait([divs1, divs2, divs3])   # Run awaitable objects concurrently until they al complete
    return divs1, divs2, divs3


if __name__ == "__main__":
    loop = asyncio.get_event_loop()     # Returns an event loop object
    # Run until the future (an eventual result of an asynchronous operation) has completed.
    d1, d2, d3 = loop.run_until_complete(main())
    pprint(d2.result())
    pprint(d3.result())
    loop.close()    # This method clears all queues and closes the event loop
