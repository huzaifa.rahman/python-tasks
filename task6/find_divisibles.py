from log_function import *
import time
from pprint import pprint


def find_divisibles(in_range, divisor):
    divisable_list = list()
    Log(in_range, divisor, 0)

    start = time.time()
    for element in range(in_range):
        if element % divisor == 0:
            divisable_list.append(element)
    end = time.time()
    
    Log(in_range, divisor, end - start)
    return divisable_list


def main():
    divs1 = find_divisibles(50800000, 34113)
    divs2 = find_divisibles(100052, 3210)
    divs3 = find_divisibles(500, 3)
    return divs1, divs2, divs3


if __name__ == "__main__":
    d1, d2, d3 = main()
    pprint(d2)
    pprint(d3)
