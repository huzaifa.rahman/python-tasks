import inspect


def Log(in_range, divisor, time):
    if time == 0:
        print(
            f"{inspect.stack()[1][3]} called with range {in_range} and divisor {divisor}")
    elif time >= 0:
        print("{} called with range {} and divisor {}. It took {:.6f} seconds".format(
            inspect.stack()[1][3], in_range, divisor, time))
