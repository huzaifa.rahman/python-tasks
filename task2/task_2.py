# ASSUMPTIOMS
# input is in the sequence of [<program_name> UP DOWN RIGHT LEFT]
# all inputs are reqired, no default values
# only positive values are allowed

import argparse
from math import sqrt

#  getting arguments
parser = argparse.ArgumentParser(description='Calculates distance between starting and final point.')   #  ArgumentParser object will hold all the information necessary to parse the command line into Python data types.
parser.add_argument("UP", type=int, help='Steps in upword direction')   #   to fill ArgumentParser object with information about program arguments
parser.add_argument("DOWN", type=int, help='Steps in downword direction')
parser.add_argument("RIGHT", type=int, help='Steps in rightword direction')
parser.add_argument("LEFT", type=int, help='Steps in leftword direction')
args = parser.parse_args()  #  strings from cmd line are stored and used when parse_args() is called 

# checking for positivity
if args.UP < 0 or args.DOWN < 0 or args.RIGHT < 0 or args.LEFT < 0:     # to check all inputs are positive
    raise Exception("Invalid input, only positive integer values are allowed")

# calculating triangle sides
perpendicular = args.UP - args.DOWN if args.UP >= args.DOWN else args.DOWN - args.UP       #    minus greater distance from smaller distance 
base = args.RIGHT - args.LEFT if args.RIGHT >= args.LEFT else args.LEFT - args.RIGHT        #   same

# calculating final distance
distance = round(sqrt(perpendicular**2 + base**2))      # from a^2 + b^2 = c^2

print(f"Final distance: {distance}")
