words = list()


def print_distinct_words_count():
    global words
    unique_count = dict()
    for index in range(len(words)):
        word = words[index]
        if word in unique_count:
            unique_count[word] += 1
        else:
            unique_count[word] = 1
    print(f"\n\n{len(unique_count)}")
    for element in unique_count:
        print(f"{unique_count[element]} ", end='')
    print("\n")


def check_sum_of_list_words():
    global words
    sum = 0
    for index in range(len(words)):
        sum += len(words[index])
        if not (sum >= 1 and sum <= 10**5):
            raise Exception(
                "ERROR! input does not fall in allowed range. Allowed range is 1 <= n <= 10^5")


def get_input_words(no_of_words):
    global words
    for index in range(no_of_words):
        word = input()
        words.append(word)
        if not word.islower():
            raise Exception("ERROR! only lowercase inputs are allowed.")


def main():
    get_input_words(int(input()))
    check_sum_of_list_words()
    print_distinct_words_count()


main()
